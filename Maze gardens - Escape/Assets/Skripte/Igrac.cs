﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Igrac : MonoBehaviour {
    
    // Energija igrača
    [HideInInspector]
    public float zivotnaEnergija = 100;
    // Količina bodova
    private int bodovi = 0;

    // Reference na Kameru, Metak, Lijevu i Desnu pušku
    public GameObject Kamera;
    public GameObject metakPrefab;
    public GameObject LijevaPuska, DesnaPuska;       
    
    // Reference na Tekst koji će se prikazivati
    private TextMesh bodoviText;
    private TextMesh zivotText;
    private TextMesh pobjednickiTekst;

    // Reference na izvorZvuka i zvučnu datoteku
    private AudioSource izvorZvuka;
    public AudioClip zvuk;

    // varijable za pucanje
    private bool pucanje = false;
    private bool lijevaPuska = false;

    // Use this for initialization
    void Start () {
        
        // Postavljanje reference na objekte sa komponentom TextMesh
        bodoviText = GameObject.Find("Bodovi").GetComponent<TextMesh>();
        zivotText = GameObject.Find("Zivot").GetComponent<TextMesh>();
        if (SceneManager.GetActiveScene().buildIndex >= 2)
        {
            pobjednickiTekst = GameObject.Find("PobjednickiTekst").GetComponent<TextMesh>();

            // postavljanje teksa 
            pobjednickiTekst.text = "";
        }else if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            transform.rotation = new Quaternion(0, -38.76f, 0, 0);
        }

        // postavljanje reference na izvor zvuka 
        izvorZvuka = GetComponent<AudioSource>();

        // postavljanje zvučne datoteke koja će se reproducirati 
        izvorZvuka.clip = zvuk;
    }

    // Funkcija za posavljanje teksta
    public void PostaviPobjecnikiTekst(string tekst)
    {
        pobjednickiTekst.text = tekst;
    }
	
	// Update is called once per frame
	void Update () {
        bodoviText.text = "Bodovi: " + bodovi;
        zivotText.text = "Zivot: " + zivotnaEnergija;        

        /* Stvaranje zrake koja se usmjerava od kamere prema naprijed ako zraka udari objekt koji ima tag "Enemy" onda treba pokrenuti
         * proces pucanja i postaviti vrijablu pucanje na true */
        RaycastHit hit;
		if(Physics.Raycast(Kamera.transform.position,Kamera.transform.forward,out hit))
        {
            if(hit.transform.tag == "Enemy")
            {
                StartCoroutine(pucaj());
                pucanje = true;
            }
            else
            {
                pucanje = false;
            }
        }

        // ako je puška usmjerena u neprijatelja započni zvuk pucanja a ako nije zaustavi
        if (pucanje)
        {
            izvorZvuka.Play();
            izvorZvuka.loop = true;
        }
        else if (!pucanje)
        {
            izvorZvuka.Stop();            
        }
	}

    // Funkcija za provjeru energije igrača
    public bool Ziv()
    {
        // ako je energija veća od 0 vrati true
        if(zivotnaEnergija > 0)
        {
            return true;
        }
        else
        {
            // ako nije provjeriti bodove i upisati ih te učitati novu scenu jer je igrač izgubio 
            PlayerPrefsManager.PostaviNajveceBodoveP(bodovi);
            SceneManager.LoadScene(0);
            return false;
        }        
    }

    // prilikom sudara s neprijateljom oduzeti određeni broj energije od igrača
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            zivotnaEnergija -= 5;
            Debug.Log(zivotnaEnergija);            
        }
    }

    /* ako je neprijatelj još uvijek u sudaru oduzimati po 0.25
     * od energije igrača svaki frame
    */
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            zivotnaEnergija -= 0.25f;
            Debug.Log(zivotnaEnergija);
        }
    }

    // funkcija koja izvršava pucanje naizmjenično iz lijeve i desno puške
    IEnumerator pucaj()
    {        
        GameObject objektMetak = Instantiate(metakPrefab);
        if (lijevaPuska)
        {           
            objektMetak.transform.position = LijevaPuska.transform.position;
            Metak metak = objektMetak.GetComponent<Metak>();
            metak.smjer = LijevaPuska.transform.forward;
            lijevaPuska = false;          
        }
        else
        {            
            objektMetak.transform.position = DesnaPuska.transform.position;
            Metak metak = objektMetak.GetComponent<Metak>();
            metak.smjer = DesnaPuska.transform.forward;
            lijevaPuska = true;            
        }
        yield return new WaitForEndOfFrame();
    }
    // funkcija za dodavanje bodova
    public void DodajBodove()
    {
        bodovi = bodovi + 10;
    }
    // funkcija za vraćanje bodova
    public int DohvatiBodove()
    {
        return bodovi;
    }

    public void PostavibodoveIZivotnuEnergiju(int bodovi, float zivotnaEnergija)
    {
        this.bodovi = bodovi;
        this.zivotnaEnergija = zivotnaEnergija;
    }
}