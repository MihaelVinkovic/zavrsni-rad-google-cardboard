﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Odabir : MonoBehaviour {

    private Camera Kamera;
    private float vrijeme = 3f;
    public GameObject L1,L2;
    public GameObject P1, P2;


    private GameObject Izlaz;



    Color bojaOdabira = Color.blue; 
    Color bojaNeOdabira = Color.white;

    Color bojaIzlaza = Color.black;
    Color bojaPrijeIzlaza = Color.red;



    private TextMesh izlazTekst;
    
  
 

	// Use this for initialization
	void Start () {
        


        Kamera = GetComponent<Camera>();
        bojaNeOdabira.a = 0;


        Izlaz = GameObject.Find("Exit");
        Izlaz.GetComponent<Renderer>().material.color = new Color(bojaPrijeIzlaza.r, bojaPrijeIzlaza.g, bojaPrijeIzlaza.b, bojaPrijeIzlaza.a);

        izlazTekst = GameObject.Find("izlazTekst").GetComponent<TextMesh>();
        izlazTekst.text = "";
     
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(vrijeme);

        RaycastHit hit;

        if (Physics.Raycast(Kamera.transform.position, Kamera.transform.forward, out hit))
        {
            if (hit.transform.name == "L1" || hit.transform.name == "L2")
            {                
                vrijeme -= Time.deltaTime;
                bojaOdabira.a = 0;
                L1.GetComponent<Renderer>().material.color = new Color(bojaOdabira.r, bojaOdabira.g, bojaOdabira.b, bojaOdabira.a);
                L2.GetComponent<Renderer>().material.color = new Color(bojaOdabira.r, bojaOdabira.g, bojaOdabira.b, bojaOdabira.a);
                izlazTekst.text = "Mod leveli: " + Mathf.Round(vrijeme).ToString();
                if (vrijeme <= 0)
                {
                    Debug.Log("Sada učitaj novu scenu");
                    SceneManager.LoadScene(2);
                }
                
            }else if(hit.transform.name != "L1" && hit.transform.name != "L2" && hit.transform.name != "P1" && hit.transform.name != "P2" && hit.transform.name != "Exit")
            {
                vrijeme = 3f;
                L1.GetComponent<Renderer>().material.color = new Color(bojaNeOdabira.r, bojaNeOdabira.g, bojaNeOdabira.b, bojaNeOdabira.a);
                L2.GetComponent<Renderer>().material.color = new Color(bojaNeOdabira.r, bojaNeOdabira.g, bojaNeOdabira.b, bojaNeOdabira.a);


                Debug.Log("Sada bi vrijeme trebalo biti 3 sekunde: " + vrijeme);
                izlazTekst.text = "";
            }


            if(hit.transform.name == "P1" || hit.transform.name == "P2")
            {
                vrijeme -= Time.deltaTime;
                bojaOdabira.a = 0;
                P1.GetComponent<Renderer>().material.color = new Color(bojaOdabira.r, bojaOdabira.g, bojaOdabira.b, bojaOdabira.a);
                P2.GetComponent<Renderer>().material.color = new Color(bojaOdabira.r, bojaOdabira.g, bojaOdabira.b, bojaOdabira.a);
                izlazTekst.text = "Mod preživljavanje: " + Mathf.Round(vrijeme).ToString();
                if (vrijeme <= 0)
                {
                    SceneManager.LoadScene(1);
                    Debug.Log("Učitaj novu scenu");
                }
            }
            else if (hit.transform.name != "L1" && hit.transform.name != "L2" && hit.transform.name != "P1"  && hit.transform.name != "P2" &&  hit.transform.name != "Exit")
            {
                P1.GetComponent<Renderer>().material.color = new Color(bojaNeOdabira.r, bojaNeOdabira.g, bojaNeOdabira.b, bojaNeOdabira.a);
                P2.GetComponent<Renderer>().material.color = new Color(bojaNeOdabira.r, bojaNeOdabira.g, bojaNeOdabira.b, bojaNeOdabira.a);

                vrijeme = 3f;
                izlazTekst.text = "";
            }



            if(hit.transform.name == "Exit")
            {
                vrijeme -= Time.deltaTime;
                Izlaz.GetComponent<Renderer>().material.color = new Color(bojaIzlaza.r, bojaIzlaza.g, bojaIzlaza.b, bojaIzlaza.a);
                izlazTekst.text = "Izlaz za: " + Mathf.Round(vrijeme).ToString();
                if(vrijeme < 0)
                {
                    Application.Quit();
                }

            }

            else if(hit.transform.name != "L1" && hit.transform.name != "L2" && hit.transform.name != "P1" && hit.transform.name != "P2" && hit.transform.name != "Exit")

            {
                Izlaz.GetComponent<Renderer>().material.color = new Color(bojaPrijeIzlaza.r, bojaPrijeIzlaza.g, bojaPrijeIzlaza.b, bojaPrijeIzlaza.a);
                izlazTekst.text = "";

            }



        }
    }
}
