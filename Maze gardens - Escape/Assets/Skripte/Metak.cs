﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Metak : MonoBehaviour {

    public float brzina = 1f;
    public Vector3 smjer;
    private float zivotniVijek = 2f;

    private KontrolerTokaIgre KontrolerToka;
   


	// Use this for initialization
	void Start () {

        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            KontrolerToka = FindObjectOfType<KontrolerTokaIgre>();
            if (KontrolerToka.KojiJeNivo() == 3)
            {
                zivotniVijek = 5f;
                Debug.Log("Koji je nivo: " + KontrolerToka.KojiJeNivo());
                Debug.Log("Zivotni vijek: " + zivotniVijek);

            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.position  += smjer * brzina * Time.deltaTime;

        zivotniVijek -= Time.deltaTime;
        if (zivotniVijek <= 0)
        {
            Destroy(gameObject);
        }

        
	}



    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            //Destroy(other.gameObject);
            Debug.Log("Pogođen je: " + other.gameObject.name);
            Destroy(gameObject);
        }

        if(other.gameObject.tag == "Povratak")
        {
            Debug.Log("UcitajPocetnu");
        }
    }

   
}
