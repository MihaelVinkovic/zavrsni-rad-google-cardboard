﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.SceneManagement;

public class Neprijatelj : MonoBehaviour {

    private int zivotnaEnergijaNeprijatelja = 100;
    private Igrac igrac;    

	// Use this for initialization
	void Start () {
        igrac = FindObjectOfType<Igrac>().GetComponent<Igrac>();
        GetComponent<AICharacterControl>().target = igrac.transform;
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Metak")
        {          
            if (zivotnaEnergijaNeprijatelja <= 0)
            {
                Destroy(gameObject);
                igrac.DodajBodove();
            }
            else
            {
                if (SceneManager.GetActiveScene().buildIndex != 2)
                {
                    zivotnaEnergijaNeprijatelja -= 10;
                }
                else if( SceneManager.GetActiveScene().buildIndex == 2)
                {
                    KontrolerTokaIgre t = new KontrolerTokaIgre();
                    if(t.KojiJeNivo() == 1)
                    {
                        zivotnaEnergijaNeprijatelja -= 10;
                    }else if(t.KojiJeNivo() == 2)
                    {
                        zivotnaEnergijaNeprijatelja -= 15;
                    }else if(t.KojiJeNivo() > 2)
                    {
                        zivotnaEnergijaNeprijatelja -= 50;
                    }
                }
            }
        }
    }

    public void PromijeniZivotnuEnergijuNeprijatelja(int zivotnaEnergijaNeprijatelja)
    {
        if (this.zivotnaEnergijaNeprijatelja != zivotnaEnergijaNeprijatelja && zivotnaEnergijaNeprijatelja >= 100)
        {
            this.zivotnaEnergijaNeprijatelja = zivotnaEnergijaNeprijatelja;
        }
    }
}
