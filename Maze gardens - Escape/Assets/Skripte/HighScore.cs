﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScore : MonoBehaviour {

    private TextMesh p;
    private TextMesh l;


	// Use this for initialization
	void Start () {
        p = GameObject.Find("P").GetComponent<TextMesh>();        
        p.text = "Rekord bodova \nPreživljavanje: " + PlayerPrefsManager.DohvatiNajveceBodoveP().ToString();
        
        l = GameObject.Find("L").GetComponent<TextMesh>();
        l.text = "Rekord bodova \nLeveli: " + PlayerPrefsManager.DohvatiNajveceBodoveL().ToString();


    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
