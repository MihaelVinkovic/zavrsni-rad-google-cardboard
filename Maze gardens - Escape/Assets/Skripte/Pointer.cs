﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pointer : MonoBehaviour {

    public GameObject Kamera;
    public GameObject metakPrefab;
    public GameObject LijevaPuska, DesnaPuska;
    private bool lijevaPuska = false;




    void Update()
    {

 




        RaycastHit hit;

        if (Physics.Raycast(Kamera.transform.position, Kamera.transform.forward, out hit))
        {
            if (hit.transform.tag == "Povratak")
            {

                StartCoroutine(pucaj());
            }
        }
    }


    IEnumerator pucaj()
    {
        GameObject objektMetak = Instantiate(metakPrefab);
        if (lijevaPuska)
        {
            objektMetak.transform.position = LijevaPuska.transform.position;

            Metak metak = objektMetak.GetComponent<Metak>();
            metak.smjer = LijevaPuska.transform.forward;
            lijevaPuska = false;


        }
        else
        {

            objektMetak.transform.position = DesnaPuska.transform.position;

            Metak metak = objektMetak.GetComponent<Metak>();
            metak.smjer = DesnaPuska.transform.forward;
            lijevaPuska = true;
        }
        yield return new WaitForEndOfFrame();
    }
}
