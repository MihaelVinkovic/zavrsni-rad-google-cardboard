﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Keeper : MonoBehaviour {

    [HideInInspector]
    public float zivot = 0;
    [HideInInspector]
    public int bodovi = 0;


	// Use this for initialization

    private void OnLevelWasLoaded(int level)
    {
        if(level >= 2)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void PostaviZivot(float zivot)
    {
        this.zivot = zivot;
    }

    public void DodajBodove(int bodovi)
    {
        this.bodovi = bodovi;
    }

}
