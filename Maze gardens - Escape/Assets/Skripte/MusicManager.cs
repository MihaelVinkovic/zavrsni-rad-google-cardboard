﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    private AudioSource audioSource;
    public AudioClip[] audioClip;
    private bool prviput = true;

	// Use this for initialization
	void Start () {
        if (prviput)
        {
            DontDestroyOnLoad(gameObject);
            prviput = false;
        }

        audioSource = GetComponent<AudioSource>();

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            audioSource.clip = audioClip[0];
            audioSource.Play();
            audioSource.loop = true;
            audioSource.volume = 0.3f;
        }

    }


    private void OnLevelWasLoaded(int level)
    {

        audioSource = GetComponent<AudioSource>();
        if (level == 0)
        {
            audioSource.clip = audioClip[0];
            audioSource.Play();
            audioSource.loop = true;
            audioSource.volume = 0.125f;
        }
        else if(level == 1)
        {
            audioSource.clip = audioClip[1];
            audioSource.Play();
            audioSource.loop = true;
            audioSource.volume = 0.125f;
        }
        else if(level == 2)
        {
            audioSource.clip = audioClip[2];
            audioSource.Play();
            audioSource.loop = true;
            audioSource.volume = 0.125f;
        }




    }



    // Update is called once per frame
    void Update () {
		
	}
}
