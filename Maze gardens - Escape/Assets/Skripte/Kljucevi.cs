﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kljucevi : MonoBehaviour {

    public SpriteRenderer[] kljucevi;

    private KontrolerTokaIgre kontroler;
    private Igrac igrac;

	// Use this for initialization
	void Start () {
        kontroler = FindObjectOfType<KontrolerTokaIgre>();
        igrac = FindObjectOfType<Igrac>();


	}
	
	// Update is called once per frame
	void Update () {
        AktivirajKljuceve();

    }

    private void AktivirajKljuceve()
    {
        if (kontroler.KojiJeNivo() >= 2 && kontroler.brojponavljanja == 1)
        {
            kljucevi[0].color = new Color(255, 255, 255, 255);
        }

        if (kontroler.KojiJeNivo() == 3 && kontroler.brojponavljanja >=1)
        {
            kljucevi[1].color = new Color(255, 255, 255, 255);
        }

        if (kontroler.KojiJeNivo() == 3 && kontroler.brojponavljanja == 2)
        {
            kljucevi[2].color = new Color(255, 255, 255, 255);
        }
    }
}
