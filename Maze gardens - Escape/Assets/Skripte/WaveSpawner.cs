﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour {

    public Transform[] pozicije;
    public GameObject[] neprijatelji;

    private int brojNeprijatelja = 0;
    private int nivo = 1;
    private bool neprijateljiPostoje = false;

    private Igrac igrac;

	// Use this for initialization
	void Start () {
        igrac = FindObjectOfType<Igrac>().GetComponent<Igrac>();        
        StartCoroutine(Igraj());       
    }

    IEnumerator Igraj()
    {
        while (igrac.Ziv())
        {
            
            for (int i = 0; i < pozicije.Length; i++)
            {
                Instantiate(neprijatelji[Random.Range(0, 3)],pozicije[i].transform.position,Quaternion.identity);        
            }
            yield return new WaitForSeconds(8);            
        }
        PlayerPrefsManager.PostaviNajveceBodoveP(igrac.DohvatiBodove());       
    }
}
