﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : MonoBehaviour {

    // ključevi koji će se koristiti prilikom spremanja rezultata
    const string NajveciBodoviLeveli = "leveliBodovi";
    const string NajveciBodoviPrezivljavanje = "prezivljavanjeBodovi";


    /* funkcija za upis bodova u datoteku ako se je dogodio slučaj
     * u kojem je igrač nadmašio prijašnji maksimalni broj bodova
     * Vrijedi za mod Leveli
    */
    public static void PostaviNajveceBodoveL(float bodovi)
    {
        // dohvačanje starog rekorda bodova i uspoređivanje
        if(bodovi > DohvatiNajveceBodoveL())
        {
            // spremanje pomoću ključa
            PlayerPrefs.SetFloat(NajveciBodoviLeveli, bodovi);
        }
    }

    // funkcija za vračanje trenutnog rekorda bodova uz pomoć ključa
    public static float DohvatiNajveceBodoveL()
    {
        return PlayerPrefs.GetFloat(NajveciBodoviLeveli);
    }

    /* funkcija za upis bodova u datoteku ako se je dogodio slučaj
     * u kojem je igrač nadmašio prijašnji maksimalni broj bodova
     * Vrijedi za mod Preživljavanje
    */
    public static void PostaviNajveceBodoveP(float bodovi)
    {
       if(bodovi > DohvatiNajveceBodoveP())
       {
           PlayerPrefs.SetFloat(NajveciBodoviPrezivljavanje, bodovi);
       }
    }
    // funkcija za vračanje trenutnog rekorda bodova uz pomoć ključa
    public static float DohvatiNajveceBodoveP()
    {
        return PlayerPrefs.GetFloat(NajveciBodoviPrezivljavanje);
    }
}
