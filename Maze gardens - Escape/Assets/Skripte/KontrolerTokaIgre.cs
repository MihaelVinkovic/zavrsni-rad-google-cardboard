﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class KontrolerTokaIgre : MonoBehaviour {
    
    // lista pozicija neprijatelja 
    public Transform[] pozicijeNeprijatelja;
    // lista neprijatelja
    public GameObject[] neprijatelji;
    // lista pozicija igrača
    //public Transform[] pozicijeIgraca;
    // referenca na igrača
    private Igrac igrac;
    private Keeper keeper;
    // broj koji označava koji je nivo
    private int nivo = 1;
    // varijabla koja vraća istinu ili laž ako postoje zombiji
    private bool imaZombija = true;
    // javna varijabla za broj ponavljanja nekog nivoa sakrivena u editoru
    [HideInInspector]
    public int brojponavljanja=0;


	// Use this for initialization
	void Start () {
        // postavljanje reference na igrača te pozivanje funkcije igra() koja započinje igru
        igrac = FindObjectOfType<Igrac>();
        // postavljanje reference na skriptu koja čuva rezultata između promjene scena
        keeper = FindObjectOfType<Keeper>();

        int razina = SceneManager.GetActiveScene().buildIndex;
        if(razina == 2)
        {
            nivo = 1;
        }else if(razina == 3)
        {
            nivo = 2;
            igrac.PostavibodoveIZivotnuEnergiju(keeper.bodovi, keeper.zivot);
        }else if(razina == 4)
        {
            nivo = 3;
            igrac.PostavibodoveIZivotnuEnergiju(keeper.bodovi, keeper.zivot);
        }  
        StartCoroutine(igra());
    }
	
	// Update is called once per frame
	void Update () {

        // provjera ako igrač nije živ
        if(!igrac.Ziv())
        {
            // ako igrač nije živ spremiti bodove
            PlayerPrefsManager.PostaviNajveceBodoveL(igrac.DohvatiBodove());
        }
        //provjeriti zombije
        ProvjeraZombija();
    }

    private void ProvjeraZombija()
    {
        // pronalazak svih objekata koji imaju oznaku "Enemy"
        if (GameObject.FindGameObjectWithTag("Enemy"))
        {
            // ako su pronađeni postavi varijablu imaZombija na istinu
            imaZombija = true;
        }
        else
        {
            // ako nema pronađenih zombija postavi varijablu nema zombija na laž
            // pozovi funkciju igra
            imaZombija = false;
            StartCoroutine(igra());
        }
    }


    public int KojiJeNivo()
    {
        // vrati nivo
        return nivo;
    }


    /* funkcija zadužena za tok igre, odnosno za stvaranje neprijatelja 
     * na određenim pozicijama te provjeru ako je igrač pobijedio
     */
    IEnumerator igra()
    {
        if (nivo == 1 && brojponavljanja <= 1)
        {
            for (int i = 0; i < 8; i++)
            {
                Instantiate(neprijatelji[Random.Range(0, 7)], pozicijeNeprijatelja[i].transform.position, Quaternion.identity);

            }
            Instantiate(neprijatelji[Random.Range(0, 7)], pozicijeNeprijatelja[9].transform.position, Quaternion.identity);           
            brojponavljanja++;
        }else if(nivo == 1 && brojponavljanja == 2)
        {            
            igrac.PostaviPobjecnikiTekst("Nastavljate na\nrazinu 2");
            //prenijeti  score i zivot u novi objekt koji se nece unistiti
            keeper.PostaviZivot(igrac.zivotnaEnergija);
            keeper.DodajBodove(igrac.DohvatiBodove());
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene(3);
        }



        if (nivo == 2 && brojponavljanja <= 1)
        {
            for (int i = 0; i < 9; i++)
            {
                Instantiate(neprijatelji[Random.Range(1, 7)], pozicijeNeprijatelja[i].transform.position, Quaternion.identity);
            }
            brojponavljanja++;
        }else if(nivo == 2 && brojponavljanja == 2)
        {
            igrac.PostaviPobjecnikiTekst("Nastavljate na\n razinu 3");
            //prenijeti score i zivot u novi objekt koji se nece unistiti
            keeper.PostaviZivot(igrac.zivotnaEnergija);
            keeper.DodajBodove(igrac.DohvatiBodove());
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene(4);
        }


        if (nivo == 3 && brojponavljanja <= 1)
        {
            for (int i = 0; i < 10; i++)
            {                            
                Instantiate(neprijatelji[Random.Range(1, 7)], pozicijeNeprijatelja[i].transform.position, Quaternion.identity);
            }
            brojponavljanja++;
        }else if(nivo == 3 && brojponavljanja == 2)
        {
            igrac.PostaviPobjecnikiTekst("Čestitke, pobjeda je vaša!");
            PlayerPrefsManager.PostaviNajveceBodoveL(igrac.DohvatiBodove());
            yield return new WaitForSeconds(5);
            SceneManager.LoadScene(0);
        }
  
        yield return null;
    }
}